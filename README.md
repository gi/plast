# PLAST - Moved to Github

PLAST has been moved to [Github](https://github.com/gi-bielefeld/plast). Please refer to the PLAST repository on Github in order to receive the tool's most recent version!
